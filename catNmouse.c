///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Waylon BadeR@todo <wbader@hawaii.edu>
/// @date   27 Jan 2022 
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>

#define DEFAULT_MAX_NUMBER 2048


int main( int argc, char* argv[] ) {
//   printf( "Cat `n Mouse\n" );
//   printf( "The number of arguments is: %d\n", argc );

   // This is an example of getting a number from a user
   // Note:  Don't enter unexpected values like 'blob' or 1.5.  
   // This also doesn't check for exceeding the max, or going under 1

   int maxNumber = DEFAULT_MAX_NUMBER;
   if(argc > 1)
   {
      int testNumber = atoi(argv[1]);
      if (testNumber <= 1)
      {
         printf("Sorry, the number you inputed is too low, use a number >1!\n");
         return 1;
      }
      maxNumber = testNumber;
   }

   int targetNumber = rand() % maxNumber + 1;
   int aGuess;

   do { 
      printf( "OK cat, I\'m thinking of a number from 1 to %d. Make a guess: ", maxNumber );
      scanf( "%d", &aGuess );

      if(aGuess < 1)
      {
         printf("You must enter a number >= 1\n");
         continue;
      }

      if(aGuess > maxNumber)
      {
         printf("You must enter a number <= %d\n", maxNumber);
         continue;
      }   

      if(aGuess == targetNumber)
      {
         printf("You got me!\n");
         printf(" |\\__/,|   (`\\\n"); 
         printf(" |_ _  |.--.) )\n");
         printf(" ( T   )     /\n"); 
         printf("(((^_(((/(((_/\n");
         break;
      }

      if(aGuess > targetNumber)
      {
         printf("No cat... the number I\'m thinking of is smaller than %d\n", aGuess); 
      }

      if(aGuess < targetNumber)
      {
         printf("No cat... the number I\'m thinking of is larger than %d\n", aGuess);
      }   


   } while (1 == 1);
   return 0;  // This is an example of how to return a 1
}

